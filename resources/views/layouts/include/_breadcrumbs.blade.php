<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>{{last(array_keys($breadCrumbs ?? []))}}</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{url('/')}}">Главная</a>
            </li>
            @foreach($breadCrumbs ?? [] as $crumb => $link)
                <li class="breadcrumb-item @if($loop->last) active @endif">
                    <a href="{{$link ? route($link) : '#'}}">{{$crumb}}</a>
                </li>
            @endforeach
        </ol>
    </div>
</div>