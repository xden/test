<div class="form-group">
    <div class="row">
        <div class="col-sm-12">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name', $model ? $model->name: '', ['placeholder' => 'Enter Name', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! Form::label('email', 'Email') !!}
            {!! Form::email('email', $model ? $model->email: '', ['placeholder' => 'Enter Email', 'class' => 'form-control']) !!}
        </div>
    </div>
    @if ($model === null)
        <div class="row">
            <div class="col-sm-12">
                {!! Form::label('password', 'Password') !!}
                {{--Form::pasword--}}
                {!! Form::text('password', $model ? $model->password: '', ['placeholder' => 'Enter Password', 'class' => 'form-control']) !!}
            </div>
        </div>
    @endif
</div>