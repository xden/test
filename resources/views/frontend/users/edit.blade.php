@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Users <div class="float-right"><a href="#" class="btn btn-sm btn-primary">Add</a></div>
                </div>
                <div class="card-body">
                    {!! Form::open(['route' => ['users.update', $model], 'method' => 'post']) !!}
                        @include('frontend.users._form', ['model' => $model])
                        <div class="text-right">
                            {!! Form::submit('Edit', ['class' => 'btn btn-primary btn-lg']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
