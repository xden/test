@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Users <div class="float-right"><a href="{{route('users.create')}}" class="btn btn-sm btn-primary">Add</a></div>
                </div>
                <div class="card-body">
                    <table class="table">
                        @forelse($models as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->created_at->format('d.m.Y H:i')}}</td>
                                <td>
                                    <a href="{{route('users.edit', $user)}}" class="btn btn-sm btn-primary">Edit</a>
                                    <a href="{{route('users.delete', $user)}}"
                                       class="btn btn-sm btn-danger"
                                       onclick="event.preventDefault();if (confirm('Вы уверены?')) { document.getElementById('delete-user-{{$user->id}}').submit();}">Delete</a>
                                    {{Form::open(['method'  => 'DELETE', 'route' => ['users.delete', $user->id], 'id' => 'delete-user-' . $user->id])}}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">not found users</td>
                            </tr>
                        @endforelse
                    </table>

                    {{$models->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
