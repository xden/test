@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    User creating
                </div>
                <div class="card-body">
                    {!! Form::open(['route' => ['users.store'], 'method' => 'post']) !!}
                        @include('frontend.users._form', ['model' => null])
                        <div class="text-right">
                            {!! Form::submit('Create', ['class' => 'btn btn-primary btn-lg']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
