<div class="form-group">
    <div class="row">
        <div class="col-sm-12">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name', $model ? $model->name: '', ['placeholder' => 'Enter Name','class' => 'form-control']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! Form::label('description', 'Description') !!}
            {!! Form::textarea('description', $model ? $model->description: '', ['placeholder' => 'Enter Description', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! Form::label('logo', 'Logo') !!}
            {!! Form::file('logo') !!}
            <br>
            @if($model && $model->logo)
                <img src="{{$model->logoPath}}">
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h2>Users</h2>
            @foreach($users as $user)
                {!! Form::checkbox('users[]', $user->id, $model && $model->users ? $model->hasUser($user) : false) !!} {{$user->name}} (<a href="mailto:{{$user->email}}">{{$user->email}}</a>) <br>
            @endforeach
        </div>
    </div>
</div>