@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Sections <div class="float-right"><a href="{{route('sections.create')}}" class="btn btn-sm btn-primary">Add</a></div>
                </div>
                <div class="card-body">
                    <table class="table">
                        @forelse($models as $section)
                            <tr>
                                <td>
                                    <img src="{{$section->logoPath}}"></td>
                                <td>
                                    <h3>{{$section->name}}</h3>
                                    <p>{{Str::limit($section->description, 50)}}</p>
                                </td>
                                <td>
                                    <h3>Users:</h3>
                                    <ul>
                                        @foreach($section->users as $user)
                                            <li>{{$loop->iteration}}. {{$user->name}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>
                                    <a href="{{route('sections.edit', $section)}}" class="btn btn-sm btn-secondary">Edit</a>
                                    <a href="{{route('sections.delete', $section)}}"
                                       class="btn btn-sm btn-danger"
                                       onclick="event.preventDefault();if (confirm('Вы уверены?')) { document.getElementsByClassName('delete-section-{{$section->id}}')[0].submit();}">Delete</a>
                                    {{Form::open(['method'  => 'DELETE', 'route' => ['sections.delete', $section->id], 'class' => 'delete-section-' . $section->id])}}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">not found sections with users</td>
                            </tr>
                        @endforelse
                    </table>

                    {{$models->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
