@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                  Sections
                </div>
                <div class="card-body">
                    {!! Form::open(['route' => ['sections.update', $model], 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                        @include('frontend.sections._form', ['model' => $model])
                        <div class="text-right">
                            {!! Form::submit('Edit', ['class' => 'btn btn-primary btn-lg']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
