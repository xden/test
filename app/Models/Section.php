<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Section
 * @package App\Models
 */
class Section extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'logo'
    ];

    /**
     * @return string
     */
    public function getLogoPathAttribute()
    {
        return asset($this->logo) ?? 'logo/nologo.png';
    }

    /**
     * @param User $user
     * @return boolean
     */
    public function hasUser(User $user) : bool
    {
        return $this->users->contains($user);
    }

    /**
     * Users in section
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'users_sections', 'section_id', 'user_id')
            ->orderBy('created_at')
            ->withTimestamps();
    }
}
