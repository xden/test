<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\BaseController;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use App\Repository\UserRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

/**
 * Class UserController
 * @package App\Http\Controllers\Main
 */
class UserController extends BaseController
{
    /**
     * @var string
     */
    protected $viewPath = 'users';

    /**
     * @var array
     */
    protected $breadCrumbs = [
        'Users' => 'users.index'
    ];

    /**
     * UserController constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        parent::__construct($repository);
    }

    /**
     * Creating User
     * @param UserCreateRequest $request
     * @return RedirectResponse
     */
    public function store(UserCreateRequest $request)
    {
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return redirect()->route('users.index');
    }

    /**
     * @param $id
     * @param UserUpdateRequest $request
     * @return RedirectResponse
     */
    public function update($id, UserUpdateRequest $request)
    {
        $user = User::findOrFail($id);

        if ($request->email !== $user->email) {
            $userWithEmailExists = User::where('email', $request->email)
                ->whereNotNull('email_verified_at')
                ->first();

            if ($userWithEmailExists) {
                // email is busy
                return redirect()->back()->withErrors(['emaill' => ['email is busy by ' . $userWithEmailExists->name]]);
            }
        }

        $user->update($request->all());

        return redirect()->route('users.edit', $user);
    }

}