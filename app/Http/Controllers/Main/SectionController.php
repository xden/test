<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\BaseController;
use App\Http\Requests\SectionRequest;
use App\Models\Section;
use App\Models\User;
use App\Repository\SectionRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\View\View;

/**
 * Class SectionController
 * @package App\Http\Controllers\Main
 */
class SectionController extends BaseController
{
    /**
     * @var string
     */
    protected $viewPath = 'sections';

    /**
     * @var array
     */
    protected $breadCrumbs = [
        'Sections' => 'sections.index'
    ];

    /**
     * SectionController constructor.
     * @param SectionRepository $repository
     */
    public function __construct(SectionRepository $repository)
    {
        parent::__construct($repository);
    }

    /**
     * @return View
     */
    public function index(): View
    {
        $models = $this->repository->with(['users'])->all();

        return $this->render('index', compact('models'));
    }

    /**
     * @return View
     */
    public function create(): View
    {
        $users = User::get();
        $this->breadCrumbs['Create'] = '';

        return $this->render('create')
            ->withUsers($users)
            ->withBreadCrumbs($this->breadCrumbs);
    }

    /**
     * @param SectionRequest $request
     * @return RedirectResponse
     */
    public function store(SectionRequest $request)
    {
        $data = $request->only('name', 'description');

        if ($request->logo) {
            // todo use better http://image.intervention.io/getting_started/installation
            $data['logo'] = $this->loadLogo($request->logo);
        }

        $section = $this->repository->store($data);

        if ($request->users) {
            $users = User::whereIn('id', $request->users)->get();
            $section->users()->sync($users->pluck('id'));
        }

        return redirect()->route('sections.index');
    }

    /**
     * @param SectionRequest $request
     */
    public function update($id, SectionRequest $request)
    {
        $section = $this->repository->with(['users'])->findOrFail($id);
        $data = $request->only('name', 'description');

        if ($request->logo) {

            if($section->logo) {
                Storage::delete(storage_path('app/public/' . $section->logo));
            }

            $data['logo'] = $this->loadLogo($request->logo);
        }

        if ($request->users) {
            $users = User::whereIn('id', $request->users)->get();
            $section->users()->sync($users->pluck('id'));
        } elseif (is_null($request->users) && $section->users->count()) {
            $section->users()->detach();
        }

        $this->repository->update($id, $data);

        return redirect()->route('sections.edit', $section);
    }

    /**
     * @param $id
     * @return View
     */
    public function edit($id) : View
    {
        $model = Section::with('users')->findOrFail($id);
        $users = User::get();

        $this->breadCrumbs['Edit ' . $model->name] = '';

        return $this->render('edit', compact('model', 'users'));
    }

    /**
     * Not best practic
     * @param $lodo
     * @return string
     */
    protected function loadLogo(UploadedFile $logo)
    {
        $pathLogo = $logo->store('public/logo');
        $path = Str::replaceFirst('public/', '', DIRECTORY_SEPARATOR . $pathLogo);
        return $path;
    }

}