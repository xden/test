<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserCreateRequest;
use App\Models\User;
use App\Repository\AbstractCrudRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class BaseController
 * @package App\Http\Controllers
 */
abstract class BaseController extends Controller
{
    /**
     * Folder views
     * @var string $viewPath
     */
    protected $viewPath = '';

    /**
     * @var array
     */
    protected $breadCrumbs = [];

    /**
     * @var AbstractCrudRepository|null
     */
    protected $repository = null;

    /**
     * CRUDController constructor.
     * @param AbstractCrudRepository $repository
     */
    public function __construct(AbstractCrudRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Page of all models
     * @return \Illuminate\View\View
     */
    public function index() : View
    {
        $models = $this->repository->all(['count' => 10]);

        return $this->render('index', compact('models'));
    }

    /**
     * @return View
     */
    public function create() : View
    {
        $this->breadCrumbs['Create'] = '';
        return $this->render('create')->withBreadCrumbs($this->breadCrumbs);
    }

    /**
     * @param $id
     * @return View
     */
    public function edit($id) : View
    {
        $model = $this->getModelById($id);

        if ($model === null) {
            abort(404);
        }

        $this->breadCrumbs['Edit ' . $model->id] = '';

        return $this->render('edit', compact('model'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id) : RedirectResponse
    {
        $this->repository->destroy($id);

        return redirect()->back();
    }

    /**
     * Get Model
     * @param int $id
     * @return Model|null
     */
    protected function getModelById(int $id) : ?Model
    {
        $model = $this->repository->get($id);
        return $model;
    }

    /**
     * @param string $view
     * @param array $data
     * @param array $breadCrumb
     * @return View
     */
    protected function render(string $view, array $data = [], array $breadCrumb = []) : View
    {
        if ($breadCrumb) {
            $this->breadCrumbs = array_merge($this->breadCrumbs, $breadCrumb);
        }

        return view('frontend.' . $this->viewPath . '.' . $view, $data)
            ->withBreadCrumbs($this->breadCrumbs);
    }
}
