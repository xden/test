<?php

namespace App\Repository;

use App\Models\Section;

/**
 * Class SectionRepository
 *
 * @package App\Repository
 */
class SectionRepository extends AbstractCrudRepository
{
    /**
     * SectionRepository constructor.
     * @param Section $model
     */
    public function __construct(Section $model)
    {
        parent::__construct($model);
    }
}
