<?php

namespace App\Repository;

use App\Models\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class UserRepository
 *
 * @package App\Repository
 */
class UserRepository extends AbstractCrudRepository
{
    /**
     * UserRepository constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    /**
     * @param array $data
     *
     * @return LengthAwarePaginator|Builder[]|Collection
     */
    public function all(array $data = [])
    {

        if (isset($data['verify'])) {
            $this->query->whereNotNull('email_verified_at');
        }


        $this->query->orderBy('id');
//        $this->query->latest();

        return parent::all($data);
    }
}
