# Работа с Docker

Все команды выполняются в директории проекта

Запуск Docker:
```bash
docker-compose up -d
```

Останавливаем Docker:
```bash
docker-compose stop
```

Выполняем команду в Docker:
```bash
docker-compose exec app composer install
docker-compose exec app php artisan migrate
```

Данные из сидеров
```bash
docker-compose exec app php artisan db:seed
```

Алиас для docker-compose (dc)
```bash
alias dc="docker-composer"
```