<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'Main\HomeController@index')->name('home');

Route::middleware('auth')->group(function(){

    /**
     * Users
     */
    Route::prefix('users')->namespace('Main')->group(function(){
        Route::get('', 'UserController@index')->name('users.index');
        Route::get('create', 'UserController@create')->name('users.create');
        Route::post('create', 'UserController@store')->name('users.store');
        Route::get('{id}', 'UserController@edit')->name('users.edit');
        Route::post('{id}', 'UserController@update')->name('users.update');
        Route::delete('{id}', 'UserController@destroy')->name('users.delete');
    });

    /**
     * Sections
     */
    Route::prefix('sections')->namespace('Main')->group(function(){
        Route::get('', 'SectionController@index')->name('sections.index');
        Route::get('create', 'SectionController@create')->name('sections.create');
        Route::post('create', 'SectionController@store')->name('sections.store');
        Route::get('{id}', 'SectionController@edit')->name('sections.edit');
        Route::post('{id}', 'SectionController@update')->name('sections.update');
        Route::delete('{id}', 'SectionController@destroy')->name('sections.delete');
    });
});