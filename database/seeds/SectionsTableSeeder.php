<?php

use Illuminate\Database\Seeder;

/**
 * Class SectionsTableSeeder
 */
class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Section::class, 15)->create()->each(function($section){
            $users = \App\Models\User::limit(rand(1,15))->get();
            $users->map(function($user) use ($section) {
                $user->sections()->attach($section->id);
            });
        });
    }
}
