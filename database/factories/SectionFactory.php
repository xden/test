<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Section;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Section::class, function (Faker $faker) {
    return [
        'name' => $faker->jobTitle(),
        'description' => $faker->text(),
        'logo' =>  $faker->imageUrl(184, 184, 'business'),
    ];
});
