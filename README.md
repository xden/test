## Test task for 4px


```bash
chmod 777 storage/ -R
```
```bash
chmod 777 bootstrap/cache/ 
```

```bash
composer install
```

```bash
cp .env.example .env
```

```bash
php artisan key:generate
```

```bash
php artisan migrate
```

```bash
php artisan db:seed
```